import { Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";
import { EmployeeController } from "./employee.controller";
import { EmployeeSchema } from './employee.model';
import { EmployeeService } from "./employee.service";

@Module({
    imports :[
        MongooseModule.forFeature([{name: "Employee" , schema: EmployeeSchema }]),
    ],
    controllers:[EmployeeController],
    providers:[EmployeeService]
})
export class EmployeeModele {}