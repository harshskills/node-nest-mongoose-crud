import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { EmployeeModele } from './Employee/employee.module';

@Module({
  imports: [
    EmployeeModele,
    MongooseModule.forRoot(
      'mongodb://localhost/nestjs',
    )
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
