import { Controller, Post, Body, Get, Param, Patch, Delete } from "@nestjs/common";
import { EmployeeService } from  './employee.service';


@Controller("employee")
export class EmployeeController {
    constructor(private readonly EmployeeService:EmployeeService ){}

    @Post()
    async AddData(
        @Body('name') name: string,
        @Body('city') city: string,
        @Body('dob') dob: string,
        @Body('phone') phone: number,
    ){
        const generatedId = await this.EmployeeService.insert(
            name,
            city,
            dob,
            phone,
          );
          return { id: generatedId };
    }

    @Get()
    async getAll() {
        const employees = await this.EmployeeService.getAll();
        return employees;
    }

    @Get(':id')
    async getProduct(@Param('id') empId: string) {
      return this.EmployeeService.getSingle(empId);
    }

    @Patch(':id')
    async update(
        @Param('id') empId: string,
        @Body('name') name: string,
        @Body('city') city: string,
        @Body('dob') dob: string,
        @Body('phone') phone: number,
    ) {
        console.log(empId , "empId")
      await this.EmployeeService.update(empId, name, city, dob, phone);
      return {"msg":"Done"};
    }

    @Delete(':id')
    async removeProduct(@Param('id') empId: string) {
        await this.EmployeeService.delete(empId);
        return {"msg":"Done"};
    }
}

